print(str(2) + ' ' +'times')
greeting1 = 'Olla'
greeting2 = 'Senior'
print(greeting1)
print(greeting2)

who = "dragon's " + 'mother'
print(who)

hryvna_per_dollar = 28
dollars_count = 10 * 1.13
hryvna_count = dollars_count * hryvna_per_dollar
print('The price is ' + str(hryvna_count) + ' hryvna')

firstname = 'Maksym'
greeting = 'Hello'
print(f'{greeting}, {firstname}!')

text = '''Текст который я 
хочу 'перенести' по
"строкам"'''
print(text)

dollars_per_euro = 1.13
hryvna_per_dollar = 28
euros_count = 10

dollars_count = euros_count * dollars_per_euro
hryvna_count = dollars_count * hryvna_per_dollar

print(hryvna_count)

king = 'King Balon the 6th'
castles = 6
rooms = 17

print(king + ' has ' + str(castles * rooms) + ' rooms.')

first_name = 'Maksym'
index = 0
print(first_name[index])

one = 'Naharis'
two = 'Mormont'
three = 'Sand'

print(one[2] + two[1] + three[3] + two[4] + two[2])

name = 'Maksym'
name_lenght = len(name)
print(name_lenght)

name = 'Maksym'
print(len(name))

print(pow(4, 2))

company1 = "Apple"
company2 = "Samsung"

print(len(company1 + company2))

num1 = -10
num2 = 13

print(abs(num1 + num2))

number = 10.1234

step1 = round(number)
print(hex(step1))